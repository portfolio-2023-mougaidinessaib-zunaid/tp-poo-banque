﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanqueEtCompte
{
    class Compte
    {
        private string titulaire;
        private int solde;
        private int numero;
        private int decouvertMax;

        public Compte(int numero, string titulaire,int solde,int decouvertMax)
        {
            this.titulaire = titulaire;
            this.solde = solde;
            this.numero = numero;
            this.decouvertMax = decouvertMax;
        }

        public int GetSolde()
        {
            return this.solde;
        }

        public string GetTitulaire()
        {
            return this.titulaire;
        }

    }
}
