﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BanqueEtCompte
{
    class Program
    {
        static void Main(string[] args)
        {
            Banque b1 = new Banque("credit lyonnais");
            Banque b2 = new Banque("Societe generale");

            Compte c1 = new Compte(1, "harry tuttle", 10000, -200);
            Compte c2 = new Compte(2,"jill layton",-100,-100);

            b1.AjouterCompte(c1);
            b2.AjouterCompte(c2);
            b1.AjouterCompte(3, "sam lowry", -1000, -500);
            b1.AjouterCompte(4, "jack lint", 200, 0);

            Console.WriteLine(b1.ValeurDepotBanque());
            Console.WriteLine(b1.CountSoldesNegatifs());
            Console.WriteLine(b1.CountSoldesPositif());
            Console.WriteLine("le compte le plus deficitaire est le compte de {0}",b1.CompteLePlusDeficitaire().GetTitulaire());
            Console.WriteLine("le compte le plus important est le compte de {0}", b1.CompteLePlusImportant().GetTitulaire());
            Console.WriteLine("les négatifs : ");
            /*List<Compte> negatifs = new List<Compte>();
            negatifs = b1.LesSoldesNegatifs();
            for (int i = 0; i < negatifs.Count; i++)
            {
                Console.WriteLine(negatifs[i].GetTitulaire());
            }*/






            //Console.WriteLine("saisir la position du compte à ajouter");
            //int index = Convert.ToInt32(Console.ReadLine());
            //b1.Supprimer(index);
            //b1.Inserer(index,c2);
            Console.ReadLine();
        }
    }
}
