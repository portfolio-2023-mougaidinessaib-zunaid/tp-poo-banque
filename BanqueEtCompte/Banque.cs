﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace BanqueEtCompte
{
    class Banque
    {
        private List<Compte> comptes;
        private string adresse;

        public Banque(string adresse)
        {
            this.adresse = adresse;
            this.comptes = new List<Compte>();
        }

        public void AjouterCompte(Compte unCompte)
        {
            this.comptes.Add(unCompte);
        }
        public void AjouterCompte(int numero, string titulaire, int solde, int decouvertMax)
        {
            Compte nouveauCompte = new Compte(numero, titulaire, solde, decouvertMax);
            this.comptes.Add(nouveauCompte);
        }
        public string Adresse
        {
            get
            {
                return this.adresse;
            }
            set
            {
                this.adresse = value;
            }

        }
        public Compte GetCompte(int index)
        {
            if (index > this.comptes.Count || index < 0)
            {
                throw new Exception("Vous êtes en dehors de la plage des valeurs de la collection");
            }
            else
            {
                return this.comptes[index];
            }
        }
        public int Count
        {
            get
            {
                return this.comptes.Count;
            }
        }
        public int GetCount()
        {
            return this.comptes.Count;
        }

        public int ValeurDepotBanque()
        {
            int cumul = 0;
            for (int i = 0; i < this.comptes.Count; i++)
            {
                cumul += this.comptes[i].GetSolde();
            }
            return cumul;
        }

        public int CountSoldesNegatifs()
        {
            int negatif = 0;
            for (int i = 0; i < this.comptes.Count; i++)
            {
                if (this.comptes[i].GetSolde() < 0)
                {
                    negatif += 1;
                }
            }
            return negatif;
        }

        public int CountSoldesPositif()
        {
            int positif = 0;
            for (int i = 0; i < this.comptes.Count; i++)
            {
                if (this.comptes[i].GetSolde() >= 0)
                {
                    positif += 1;
                }
            }
            return positif;
        }

        public Compte CompteLePlusDeficitaire()
        {
            int indexCompteDefcitaire = 0;
            int soldeDeficitaire = this.comptes[0].GetSolde();
            for (int i = 1; i < this.comptes.Count; i++)
            {
                if (this.comptes[i].GetSolde() < soldeDeficitaire)
                {
                    indexCompteDefcitaire = i;
                }
            }
            return this.comptes[indexCompteDefcitaire];
        }

        public Compte CompteLePlusImportant()
        {
            int indexCompteImportant = 0;
            int soldeImportant = this.comptes[0].GetSolde();
            for (int i = 1; i < this.comptes.Count; i++)
            {
                if (this.comptes[i].GetSolde() > soldeImportant)
                {
                    indexCompteImportant = i;
                }
            }
            return this.comptes[indexCompteImportant];
        }

        public List<Compte> LesSoldesNegatifs()
        {
            List<Compte> LesComptesNegatif = new List<Compte>();
            for (int i = 0; i < this.comptes.Count; i++)
            {
                if (this.comptes[i].GetSolde() < 0)
                {
                    LesComptesNegatif.Add((this.comptes[i]));
                    
                }
            }
            return LesComptesNegatif;
        }

        public void Supprimer(int index)

        {
            if (index > this.comptes.Count || index < 0)
            {
                throw new Exception("Vous êtes en dehors de la plage des valeurs de la collection");
            }
            else
            {
                 this.comptes.RemoveAt(index);
            }
            for (int i = 0; i < this.comptes.Count; i++)
            {
                Console.WriteLine(this.comptes[i].GetTitulaire());
            }
        }

        public void Inserer(int index,Compte c)
        {
            if (index > this.comptes.Count+1 || index < 0)
            {
                throw new Exception("Vous êtes en dehors de la plage des valeurs de la collection");
            }
            else
            {
                this.comptes.Insert(index,c);
            }
            for (int i = 0; i < this.comptes.Count; i++)
            {
                Console.WriteLine(this.comptes[i].GetTitulaire());
            }
        }
    }
}
