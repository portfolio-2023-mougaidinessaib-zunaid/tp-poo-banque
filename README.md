# TP POO BANQUE

Une banque possède plusieurs comptes.

![diagramme](img/diagramme.PNG)

Nous allons créer la classe :  

![classe](img/tutoclasse.PNG)
puis nommer la classe :  
![classe2](img/tutoclasse2.PNG)

Nous pouvons désormais déclarer des données membres :  
```csharp
private int numero;
private string titulaire;
private int solde;
private int decouvertMax;
```

Constructeur :
```csharp
public Compte(int numero, string titulaire,int solde,int decouvertMax)
        {
            this.titulaire = titulaire;
            this.solde = solde;
            this.numero = numero;
            this.decouvertMax = decouvertMax;
        }
```

On peut ensuite créer des accesseurs permettant de récupérer ou de modifier les données membres : 
```csharp
public int Solde
{
    get{
        return this.solde;
    }
    set{
        this.solde=value;
    }
}
```


Dans le fichier Programm.cs, nous pouvons créer et manipuler un objet Compte.  
Pour créer un compte :  
```csharp
Compte c1 = new Compte(1, "harry tuttle", 10000, -200);
```

Nous pouvons ensuite modifier les valeurs qui ont été attribuées à l'objet, par exemple :  
```csharp
c1.Solde=15 000;
```
Ou encore obtenir une de ses données :  
```csharp
int budget=c1.Solde;
Console.Write(budget);
// renvoie 15 000
```
<br><br>

Pour finir, nous savons qu'un compte est lié à une seule banque, et qu'une banque a plusieurs comptes.  
Pour retranscrire cela, on utilise dans la classe Banque : une collections d'objets.
```csharp
private List<Compte> comptes;
```
Ainsi chaque objet Banque aura une multitude de comptes.